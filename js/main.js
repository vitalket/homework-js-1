/* 1. Через var let const */

/* 2. У функції prompt користувач вводить данні потім з ними щось робимо,
 а у функції confirm користувач може натиснути так / ні, функція поверне true або false  */

 /* 3. Неявне перетворення типів це ті перетворення які відбуваються в процесі роботи якихось операторів типи яких ми явно не задаємо. 
          В третьому прикладі вводимо число, а функція prompt повертає рядок,
          ще один приклад console.log(4 + "4"), получается 44, а не 8, тобто 4 стало рядком */

 let admin;
 let name = "Vitaliy";
 admin = name;
 console.log(admin);

 let days = 7;
 console.log(`${days * 60}s`);

 let userNumber = prompt("Введіть будь-ласка число");
 console.log(userNumber);
 console.log(typeof userNumber);


